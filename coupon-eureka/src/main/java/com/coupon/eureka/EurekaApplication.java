package com.coupon.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author      王哲
 * @Contact     1121586359@qq.com
 * @ClassName   EurekaApplication.java
 * @create      2023年06月23日 下午7:52
 * @Description EurekaServer
 * @Version     V1.0
 */

@SpringBootApplication
@EnableEurekaServer
public class EurekaApplication {

    public static void main(String[] args) {

        SpringApplication.run(EurekaApplication.class,args);

    }

}
