package com.coupon.template.converter;

import com.coupon.common.constant.DistributeTargetEnum;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName DistributeTargetEnumConverter.java
 * @create 2023年06月25日 上午11:32
 * @Description 目标类型枚举转换类
 * @Version V1.0
 */
@Converter
public class DistributeTargetEnumConverter implements
        AttributeConverter<DistributeTargetEnum,Integer> {
    @Override
    public Integer convertToDatabaseColumn(DistributeTargetEnum distributeTargetEnum) {
        return distributeTargetEnum.getCode();
    }

    @Override
    public DistributeTargetEnum convertToEntityAttribute(Integer code) {
        return DistributeTargetEnum.of(code);
    }
}
