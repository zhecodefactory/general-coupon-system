package com.coupon.template.entity;

import com.coupon.common.constant.CouponCategoryEnum;
import com.coupon.common.constant.DistributeTargetEnum;
import com.coupon.common.constant.ProductLineEnum;
import com.coupon.common.vo.TemplateRule;
import com.coupon.template.converter.CouponCategoryConverter;
import com.coupon.template.converter.DistributeTargetEnumConverter;
import com.coupon.template.converter.ProductLineEnumConverter;
import com.coupon.template.converter.TemplateRuleConverter;
import com.coupon.template.serialization.CouponTemplateSerialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName CouponTemplate.java
 * @create 2023年06月25日 上午10:50
 * @Description 优惠券模板实体类定义: 基础属性 + 规则属性
 * @Version V1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "coupon_template")
@JsonSerialize(using = CouponTemplateSerialize.class)
public class CouponTemplate implements Serializable {

    // 自增主键
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",nullable = false)
    @Basic
    private Integer id;

    // 是否是可用状态
    @Column(name = "available",nullable = false)
    private Boolean available;

    // 是否过期
    @Column(name = "expired",nullable = false)
    private Boolean expired;

    // 优惠券名称
    @Column(name = "name",nullable = false)
    private String name;

    // 优惠券logo
    @Column(name = "logo",nullable = false)
    private String logo;

    // 优惠券描述
    @Column(name = "intro",nullable = false)
    private String desc;

    // 优惠券分类
    @Column(name = "category",nullable = false)
    @Convert(converter = CouponCategoryConverter.class)
    private CouponCategoryEnum category;

    // 业务线
    @Column(name = "product_line",nullable = false)
    @Convert(converter = ProductLineEnumConverter.class)
    private ProductLineEnum productLine;

    // 优惠券总数
    @Column(name = "coupon_count",nullable = false)
    private Integer count;

    // 创建时间
    @CreatedDate
    @Column(name = "create_time",nullable = false)
    private Date createTime;

    // 创建用户
    @Column(name = "user_id",nullable = false)
    private Long userId;

    // 优惠券模板的编码
    @Column(name = "template_key",nullable = false)
    private String key;

    // 目标用户
    @Column(name = "target",nullable = false)
    @Convert(converter = DistributeTargetEnumConverter.class)
    private DistributeTargetEnum target;

    // 优惠券规则
    @Column(name = "rule",nullable = false)
    @Convert(converter = TemplateRuleConverter.class)
    private TemplateRule rule;

    // 自定义构造函数
    public CouponTemplate(String name, String logo, String desc,
                          String category, Integer productLine,
                          Integer count, Long userId, Integer target,
                          TemplateRule rule) {
        this.available = false;
        this.expired = false;
        this.name = name;
        this.logo = logo;
        this.desc = desc;
        this.category = CouponCategoryEnum.of(category);
        this.productLine = ProductLineEnum.of(productLine);
        this.count = count;
        this.userId = userId;
        this.target = DistributeTargetEnum.of(target);
        this.rule = rule;
        // 优惠券唯一编码 = 4（业务线和类型） + 8（日期） + id（扩充为4位）
        this.key = productLine.toString() + category +
                new SimpleDateFormat("yyyyMMdd").format(new Date());
    }

}
