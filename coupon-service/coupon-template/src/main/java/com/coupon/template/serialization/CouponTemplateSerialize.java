package com.coupon.template.serialization;

import com.alibaba.fastjson.JSON;
import com.coupon.template.entity.CouponTemplate;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.text.SimpleDateFormat;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName CouponTemplateSerialize.java
 * @create 2023年06月25日 下午1:44
 * @Description 优惠券模板实体自定义序列化器
 * @Version V1.0
 */

public class CouponTemplateSerialize extends
        JsonSerializer<CouponTemplate> {

    @Override
    public void serialize(CouponTemplate couponTemplate,
                          JsonGenerator jsonGenerator,
                          SerializerProvider serializerProvider) throws IOException {

        // 开始序列化对象
        jsonGenerator.writeStartObject();

        jsonGenerator.writeStringField("id", couponTemplate.getId().toString());
        jsonGenerator.writeStringField("name", couponTemplate.getName());
        jsonGenerator.writeStringField("logo", couponTemplate.getLogo());
        jsonGenerator.writeStringField("desc", couponTemplate.getDesc());
        jsonGenerator.writeStringField("category",
                couponTemplate.getCategory().getDescription());
        jsonGenerator.writeStringField("productLine",
                couponTemplate.getProductLine().getDescription());
        jsonGenerator.writeStringField("count",couponTemplate.getCount().toString());
        jsonGenerator.writeStringField("createTime",
                new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                        .format(couponTemplate.getCreateTime()));
        jsonGenerator.writeStringField("userId",couponTemplate.getUserId().toString());
        jsonGenerator.writeStringField("key",
                couponTemplate.getKey() + String.format("%04d", couponTemplate.getId()));
        jsonGenerator.writeStringField("target",
                couponTemplate.getTarget().getDescription());
        jsonGenerator.writeStringField("rule", JSON.toJSONString(couponTemplate.getRule()));

        // 结束序列化对象
        jsonGenerator.writeEndObject();

    }
}
