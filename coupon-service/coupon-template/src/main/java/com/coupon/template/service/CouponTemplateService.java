package com.coupon.template.service;

import com.coupon.common.exception.CouponException;
import com.coupon.template.entity.CouponTemplate;
import com.coupon.template.vo.CouponTemplateRequestVO;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName CouponTemplateService.java
 * @create 2023年06月25日 下午2:32
 * @Description 构建优惠券模板接口定义
 * @Version V1.0
 */

public interface CouponTemplateService {

    /**
     * 根据优惠券模板请求对象构建优惠券模板
     * @param template {@link CouponTemplateRequestVO} 优惠券模板请求对象
     * @return {@link CouponTemplate} 优惠券模板实体
     * @throws CouponException 优惠券异常
     */
    CouponTemplate buildTemplate(CouponTemplateRequestVO template) throws CouponException;

}
