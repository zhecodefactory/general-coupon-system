package com.coupon.template.converter;

import com.coupon.common.constant.ProductLineEnum;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName ProductLineEnumConverter.java
 * @create 2023年06月25日 上午11:30
 * @Description 业务线枚举转换类
 * @Version V1.0
 */
@Converter
public class ProductLineEnumConverter implements
        AttributeConverter<ProductLineEnum,Integer> {
    @Override
    public Integer convertToDatabaseColumn(ProductLineEnum productLineEnum) {
        return productLineEnum.getCode();
    }

    @Override
    public ProductLineEnum convertToEntityAttribute(Integer code) {
        return ProductLineEnum.of(code);
    }
}
