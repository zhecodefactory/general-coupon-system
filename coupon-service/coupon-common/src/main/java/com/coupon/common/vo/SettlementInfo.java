package com.coupon.common.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName SettlementInfo.java
 * @create 2023年06月26日 上午11:11
 * @Description 结算信息 对象定义 包含了用户id 商品信息 优惠券列表 结算结果金额
 * @Version V1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SettlementInfo {

    // 用户id
    private Long userId;

    // 优惠券列表
    private List<CouponAndTemplateInfo> couponAndTemplateInfos;

    // 商品信息
    private List<GoodsInfo> goodsInfos;

    // 结果结算金额
    private Double cost;

    // 是否使结算生效，即核销
    private Boolean employ;

    // 优惠券 和 优惠券模板信息
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CouponAndTemplateInfo {

        // Coupon的主键
        private Integer id;

        // 优惠券对应的模板对象
        private CouponTemplateSDK templateSDK;

    }

}
