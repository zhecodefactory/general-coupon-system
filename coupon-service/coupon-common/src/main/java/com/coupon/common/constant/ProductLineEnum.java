package com.coupon.common.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Objects;
import java.util.stream.Stream;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName ProductLineEnum.java
 * @create 2023年06月25日 上午9:56
 * @Description 业务线枚举
 * @Version V1.0
 */
@Getter
@AllArgsConstructor
public enum ProductLineEnum {

    DAMAO("大猫", 1),
    DABAO("大宝", 2);

    // 业务线描述
    private String description;
    // 业务线编码
    private Integer code;

    public static ProductLineEnum of(Integer code){
        Objects.requireNonNull(code);

        return Stream.of(values())
                .filter(bean -> bean.code.equals(code))
                .findAny()
                .orElseThrow(() -> new IllegalArgumentException(code + " not exists!"));
    }

}
