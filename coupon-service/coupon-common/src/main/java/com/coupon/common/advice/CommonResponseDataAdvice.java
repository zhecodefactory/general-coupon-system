package com.coupon.common.advice;

import com.coupon.common.annotation.IgnoreResponseAdvice;
import com.coupon.common.vo.CommonResponse;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName CommonResponseDataAdvice.java
 * @create 2023年06月24日 下午4:23
 * @Description 统一响应数据处理
 * @Version V1.0
 */
@RestControllerAdvice
@SuppressWarnings("all")
public class CommonResponseDataAdvice implements ResponseBodyAdvice<Object> {

    // 判断是否需要对响应进行处理
    @Override
    public boolean supports(MethodParameter methodParameter,
                            Class<? extends HttpMessageConverter<?>> aClass) {

        // 如果当前方法所在的类标识了 @IgnoreResponseAdvice 注解，不需要处理
        if (methodParameter.getDeclaringClass().isAnnotationPresent(
                IgnoreResponseAdvice.class
        )) {
            return false;
        }

        // 如果当前方法标识了 @IgnoreResponseAdvice 注解，不需要处理
        if (methodParameter.getMethod().isAnnotationPresent(
                IgnoreResponseAdvice.class
        )) {
            return false;
        }

        // 对响应进行处理，执行 beforeBodyWrite 方法
        return true;
    }

    /**
     * 响应返回之前的处理
     * @param o 响应对象
     * @param methodParameter 当前控制器方法的参数
     * @param mediaType 当前请求的响应类型
     * @param aClass 当前选择的 HttpMessageConverter 的实现类
     * @param serverHttpRequest 当前的 Http 请求对象
     * @param serverHttpResponse 当前的 Http 响应对象
     * @return 处理后的响应对象
     */
    @Override
    public Object beforeBodyWrite(Object o, MethodParameter methodParameter,
                                  MediaType mediaType, Class<? extends HttpMessageConverter<?>> aClass,
                                  ServerHttpRequest serverHttpRequest, ServerHttpResponse serverHttpResponse) {
        // 定义最终的返回对象
        CommonResponse<Object> response = new CommonResponse<>(0, "");
        // 如果 o 是 null，response 不需要设置 data
        if (null == o) {
            return response;
            // 如果 o 已经是 CommonResponse，不需要再次处理
        } else if (o instanceof CommonResponse) {
            response = (CommonResponse<Object>) o;
            // 否则，把响应对象作为 CommonResponse 的 data 部分
        } else {
            response.setData(o);
        }

        return null;
    }
}
