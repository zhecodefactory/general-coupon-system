package com.coupon.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName IgnoreResponseAdvice.java
 * @create 2023年06月24日 下午4:21
 * @Description 忽略统一响应注解定义
 * @Version V1.0
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface IgnoreResponseAdvice {

}
