package com.coupon.common.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Objects;
import java.util.stream.Stream;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName CouponCategory.java
 * @create 2023年06月25日 上午9:45
 * @Description 优惠券分类 枚举
 * @Version V1.0
 */
@Getter
@AllArgsConstructor
public enum CouponCategoryEnum {

    // 满减券
    MANJIAN("满减券", "001"),
    // 折扣券
    ZHEKOU("折扣券",  "002"),
    // 立减券
    LIJIAN("立减券",  "003");


    // 优惠券分类描述信息
    private String description;

    // 优惠券分类编码
    private String code;

    // 根据code获取到CouponCategory
    public static CouponCategoryEnum of(String code) {

        Objects.requireNonNull(code);

        return Stream.of(values())
                .filter(bean -> bean.code.equals(code))
                .findAny()
                .orElseThrow(() -> new IllegalArgumentException(code + " not exists!"));
    }

}
