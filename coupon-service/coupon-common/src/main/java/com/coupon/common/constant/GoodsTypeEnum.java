package com.coupon.common.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Objects;
import java.util.stream.Stream;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName GoodsTypeEnum.java
 * @create 2023年06月26日 上午11:03
 * @Description 商品类型枚举
 * @Version V1.0
 */
@Getter
@AllArgsConstructor
public enum GoodsTypeEnum {

    WENYU(1,"文娱"),
    SHENGXIAN(2,"生鲜"),
    JIAJU(3,"家居"),
    OTHERS(4,"其他"),
    ALL(5,"全品类");


    // 普通商品类型
    private Integer code;

    // 商品类型描述
    private String description;

    // 根据code获取到GoodsTypeEnum
    public static GoodsTypeEnum of(Integer code) {
        Objects.requireNonNull(code);
        return Stream.of(values())
                .filter(bean -> bean.code.equals(code))
                .findAny()
                .orElseThrow(() -> new IllegalArgumentException(code + " not exists!"));
    }

}
