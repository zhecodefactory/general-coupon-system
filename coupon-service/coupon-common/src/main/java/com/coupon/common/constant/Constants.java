package com.coupon.common.constant;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName Constants.java
 * @create 2023年06月25日 下午3:29
 * @Description 通用常量定义类
 * @Version V1.0
 */
public class Constants {

    // Kafka 消息的 Topic
    public static final String TOPIC = "user_coupon_op";

    // Redis Key 前缀定义
    public static class RedisPrefix {

        // 优惠券码 key 前缀
        public static final String COUPON_TEMPLATE = "coupon_template_code_";

        // 用户当前所有可用的优惠券 key 前缀
        public static final String USER_COUPON_USABLE = "user_coupon_usable_";

        // 用户当前所有已使用的优惠券 key 前缀
        public static final String USER_COUPON_USED = "user_coupon_used_";

        // 用户当前所有已过期的优惠券 key 前缀
        public static final String USER_COUPON_EXPIRED = "user_coupon_expired_";
    }

}
