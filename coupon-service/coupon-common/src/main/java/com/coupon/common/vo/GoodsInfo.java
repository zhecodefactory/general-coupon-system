package com.coupon.common.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName GoodsInfo.java
 * @create 2023年06月26日 上午11:08
 * @Description 商品信息
 * @Version V1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GoodsInfo {

    // 商品类型 {@link com.coupon.common.constant.GoodsTypeEnum}
    private Integer type;

    // 商品价格
    private Double price;

    // 商品数量
    private Integer count;

}
