package com.coupon.distribution.feign;

import com.coupon.common.vo.CommonResponse;
import com.coupon.common.vo.CouponTemplateSDK;
import com.coupon.distribution.feign.hystrix.CouponTemplateClientHystrix;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName CouponTemplateClient.java
 * @create 2023年06月26日 下午5:40
 * @Description 优惠券模板微服务Feign接口定义
 * @Version V1.0
 */
@FeignClient(value = "eureka-client-coupon-template",
        fallback = CouponTemplateClientHystrix.class)
public interface CouponTemplateClient {

    /**
     * 查找所有可用的优惠券模板
     */
    @GetMapping(value = "/coupon-template/template/sdk/all")
    CommonResponse<List<CouponTemplateSDK>> findAllUsableTemplate();

    /**
     * 获取模板ids到CouponTemplateSDK的映射
     * @param ids
     * @return
     */
    @GetMapping("/coupon-template/template/sdk/infos")
    CommonResponse<Map<Integer, CouponTemplateSDK>> findIds2TemplateSDK(
            @RequestParam List<Integer> ids
    );
}
