package com.coupon.distribution;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.web.client.RestTemplate;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName DistributionApplication.java
 * @create 2023年06月26日 上午9:10
 * @Description 分发微服务的启动入口
 * @Version V1.0
 */

/**
 * @SpringBootApplication：这个注解是SpringBoot的核心注解，包含了自动配置、组件扫描等功能，相当于一个快速启动器。
 *
 * @EnableEurekaClient：这是一个标记注解，用于开启Eureka客户端功能，使应用程序能够注册到Eureka服务注册中心。
 *
 * @EnableFeignClients：这个注解开启了Feign客户端功能，可以方便地使用声明式HTTP客户端调用其他的RESTful服务。
 *
 * @EnableCircuitBreaker：这个注解开启了断路器功能，使服务在出现故障或者超时时，能够更好地保护系统稳定性和可用性。
 *
 * @EnableJpaAuditing：这个注解开启了JPA审计功能，使实体类自动填充创建时间、更新时间等时间戳字段。
 */
@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
@EnableCircuitBreaker
@EnableJpaAuditing
public class DistributionApplication {

    @Bean
    @LoadBalanced // 使RestTemplate具备负载均衡的能力
    RestTemplate restTemplate(){
        return new RestTemplate();
    }


    public static void main(String[] args) {
        SpringApplication.run(DistributionApplication.class, args);
    }

}
