package com.coupon.distribution.feign.hystrix;

import com.coupon.common.vo.CommonResponse;
import com.coupon.common.vo.CouponTemplateSDK;
import com.coupon.distribution.feign.CouponTemplateClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName CouponTemplateClientHystrix.java
 * @create 2023年06月26日 下午7:25
 * @Description 优惠券模板 Feign接口调用 熔断降级策略
 * @Version V1.0
 */
@Slf4j
@Component
public class CouponTemplateClientHystrix implements CouponTemplateClient {

    /**
     * 查找所有可用的优惠券模板
     */
    @Override
    public CommonResponse<List<CouponTemplateSDK>> findAllUsableTemplate() {

        log.error("[eureka-client-coupon-template] findAllUsableTemplate " +
                "request error");

        List<CouponTemplateSDK> couponTemplateSDKS = new ArrayList<>();

        return new CommonResponse<>(-1,
                "[eureka-client-coupon-template] findAllUsableTemplate " +
                        "request error",
                couponTemplateSDKS);
    }

    /**
     * 获取模板ids到CouponTemplateSDK的映射
     *
     * @param ids
     * @return
     */
    @Override
    public CommonResponse<Map<Integer, CouponTemplateSDK>> findIds2TemplateSDK(List<Integer> ids) {
        log.error("[eureka-client-coupon-template] findIds2TemplateSDK " +
                "request error");

        return new CommonResponse<>(-1,
                "[eureka-client-coupon-template] findIds2TemplateSDK " +
                        "request error",
                new HashMap<>());
    }
}
