package com.coupon.distribution.service;

import com.coupon.common.exception.CouponException;
import com.coupon.distribution.entity.Coupon;

import java.util.List;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName RedisService.java
 * @create 2023年06月26日 上午10:28
 * @Description Redis相关的操作服务接口定义
 *              1.用户的三个状态优惠券 Cache 操作
 *              2.优惠券模板生成的优惠券码 Cache 操作
 * @Version V1.0
 */
public interface RedisService {

    // 根据userId和状态找到缓存的优惠券列表数据
    List<Coupon> getCachedCoupons(Long userId, Integer status);

    // 保存空的优惠券列表到缓存中
    void saveEmptyCouponListToCache(Long userId, List<Integer> status);

    // 尝试从Cache中获取一个优惠券码
    String tryToAcquireCouponCodeFromCache(Integer templateId);

    // 将优惠券保存到Cache中
    Integer addCouponToCache(Long userId, List<Coupon> coupons, Integer status)
            throws CouponException;

}
