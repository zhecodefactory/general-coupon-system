package com.coupon.distribution.controller;

import com.coupon.common.exception.CouponException;
import com.coupon.common.vo.CouponTemplateSDK;
import com.coupon.common.vo.SettlementInfo;
import com.coupon.distribution.entity.Coupon;
import com.coupon.distribution.service.UserService;
import com.coupon.distribution.vo.AcquireTemplateRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName CouponDistributionController.java
 * @create 2023年06月27日 上午9:00
 * @Description 优惠券分发接口
 * @Version V1.0
 */
@Slf4j
@RestController
public class CouponDistributionController {

    @Autowired
    private UserService userService;

    // 用户三类状态优惠券信息展示服务
    // http://localhost:7001/coupon-distribution/coupon-distribution/coupons
    // http://localhost:9000/coupon-distribution/coupons
    @GetMapping("/coupons")
    public List<Coupon> coupons(@RequestParam Long userId,@RequestParam Integer status)
            throws CouponException {
        return userService.findCouponByStatus(userId,status);
    }

    // 查看用户当前可以领取的优惠券模板
    // http://localhost:7001/coupon-distribution/coupon-distribution/template
    // http://localhost:9000/coupon-distribution/template
    @GetMapping("/template")
    public List<CouponTemplateSDK> template(@RequestParam Long userId)
            throws CouponException {
        return userService.findAvailableTemplate(userId);
    }

    // 用户领取优惠券服务
    // http://localhost:7001/coupon-distribution/coupon-distribution/acquire/template
    // http://localhost:9000/coupon-distribution/acquire/template
    @GetMapping("/acquire/template")
    public Coupon acquireTemplate(AcquireTemplateRequest request)
            throws CouponException {
        return userService.acquireTemplate(request);
    }

    // 用户消费优惠券服务
    // http://localhost:7001/coupon-distribution/coupon-distribution/settlement
    // http://localhost:9000/coupon-distribution/settlement
    @GetMapping("/settlement")
    public SettlementInfo settlement(SettlementInfo info)
            throws CouponException {
        return userService.settlement(info);
    }

}
