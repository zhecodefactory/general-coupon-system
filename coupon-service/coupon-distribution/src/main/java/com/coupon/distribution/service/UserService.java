package com.coupon.distribution.service;

import com.coupon.common.exception.CouponException;
import com.coupon.common.vo.CouponTemplateSDK;
import com.coupon.common.vo.SettlementInfo;
import com.coupon.distribution.entity.Coupon;
import com.coupon.distribution.vo.AcquireTemplateRequest;

import java.util.List;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName UserService.java
 * @create 2023年06月26日 上午10:49
 * @Description 用户服务相关接口定义
 *              1.用户三类状态优惠券信息展示服务
 *              2.查看用户当前可以领取的优惠券模板 - coupon-template微服务配合实现
 *              3.用户领取优惠券服务
 *              4.用户消费优惠券服务 - coupon-settlement微服务配合实现
 * @Version V1.0
 */
public interface UserService {

    // 用户三类状态优惠券信息展示服务
    List<Coupon> findCouponByStatus(Long userId,Integer status)
            throws CouponException;

    // 查看用户当前可以领取的优惠券模板
    List<CouponTemplateSDK> findAvailableTemplate(Long userId)
            throws CouponException;

    // 用户领取优惠券服务
    Coupon acquireTemplate(AcquireTemplateRequest request)
            throws CouponException;

    // 用户消费优惠券服务
    // coupon-settlement微服务配合实现
    SettlementInfo settlement(SettlementInfo info)
            throws CouponException;

}
