package com.coupon.distribution.converter;

import com.coupon.distribution.constant.CouponStatusEnum;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName CouponStatusEnumConverter.java
 * @create 2023年06月26日 上午9:43
 * @Description 优惠券使用状态枚举属性转换器类
 * @Version V1.0
 */
@Converter
public class CouponStatusEnumConverter implements AttributeConverter<CouponStatusEnum, Integer>  {
    @Override
    public Integer convertToDatabaseColumn(CouponStatusEnum couponStatusEnum) {
        return couponStatusEnum.getCode();
    }

    @Override
    public CouponStatusEnum convertToEntityAttribute(Integer code) {
        return CouponStatusEnum.of(code);
    }
}
