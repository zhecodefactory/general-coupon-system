package com.coupon.settlement.executor;

import com.coupon.common.vo.SettlementInfo;
import com.coupon.settlement.constant.RuleFlagEnum;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName RuleExecutor.java
 * @create 2023年06月27日 上午11:06
 * @Description 优惠券模板规则处理器接口定义
 * @Version V1.0
 */
public interface RuleExecutor {

    /**
     * 规则类型标记
     * @return {@link RuleFlagEnum}
     */
    RuleFlagEnum ruleConfig();


    /**
     * 优惠券规则计算
     * @param settlement {@link SettlementInfo} 包含了选择的优惠券
     * @return {@link SettlementInfo} 修正过的结算信息
     */
    SettlementInfo computeRule(SettlementInfo settlement);

}
