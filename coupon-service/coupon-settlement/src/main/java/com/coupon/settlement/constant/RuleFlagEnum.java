package com.coupon.settlement.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName RuleFlagEnum.java
 * @create 2023年06月27日 上午11:02
 * @Description 规则类型枚举定义
 * @Version V1.0
 */
@Getter
@AllArgsConstructor
public enum RuleFlagEnum {

    // 单类别优惠券定义
    MANJIAN("满减券的计算规则"),
    ZHEKOU("折扣券的计算规则"),
    LIJIAN("立减券的计算规则"),

    // 多类别优惠券定义
    MANJIAN_ZHEKOU("满减券+折扣券的计算规则");

    // 规则描述
    private String description;



}
