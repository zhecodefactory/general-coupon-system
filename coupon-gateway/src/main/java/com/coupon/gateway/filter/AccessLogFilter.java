package com.coupon.gateway.filter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName AccessLogFilter.java
 * @create 2023年06月23日 下午10:03
 * @Description 请求日志过滤器
 * @Version V1.0
 */
@Component
@Slf4j
@SuppressWarnings("all")
public class AccessLogFilter extends AbstractPostZuulFilter {

    @Override
    protected Object cRun() {
        HttpServletRequest request = requestContext.getRequest();

        // 从PreRequestFilter 中获取进入时的时间戳
        Long startTime = (Long) requestContext.get("startTime");
        String requestURI = request.getRequestURI();
        long useTime = System.currentTimeMillis() - startTime;

        //从网关通过的请求都会打印日志记录
        log.info("请求uri:{},方法执行时间：{}",requestURI,useTime);

        return success();
    }

    @Override
    public int filterOrder() {
        return FilterConstants.SEND_RESPONSE_FILTER_ORDER - 1;
    }
}
