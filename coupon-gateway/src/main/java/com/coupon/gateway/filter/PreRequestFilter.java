package com.coupon.gateway.filter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName PreRequestFilter.java
 * @create 2023年06月23日 下午10:01
 * @Description 前置请求过滤器 存储客户端进入时的请求时间
 * @Version V1.0
 */
@Component
@Slf4j
@SuppressWarnings("all")
public class PreRequestFilter extends AbstractPreZuulFilter{

    @Override
    protected Object cRun() {
        requestContext.set("startTime",System.currentTimeMillis());
        return success();
    }

    @Override
    public int filterOrder() {
        return 0;
    }
}
