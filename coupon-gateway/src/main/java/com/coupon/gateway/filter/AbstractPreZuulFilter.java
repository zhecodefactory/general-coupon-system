package com.coupon.gateway.filter;

import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName AbstractPreZuulFilter.java
 * @create 2023年06月23日 下午9:13
 * @Description 前置抽象过滤器类
 * @Version V1.0
 */
public abstract class AbstractPreZuulFilter extends AbstractZuulFilter{

    @Override
    public String filterType() {
        return FilterConstants.PRE_TYPE;
    }
}
